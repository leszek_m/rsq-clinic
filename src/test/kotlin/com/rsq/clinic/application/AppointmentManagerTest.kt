package com.rsq.clinic.application

import com.rsq.clinic.application.dto.AppointmentRequest
import com.rsq.clinic.domain.AppointmentService
import com.rsq.clinic.domain.DoctorService
import com.rsq.clinic.domain.PatientService
import com.rsq.clinic.domain.exceptions.AppointmentCreationException
import com.rsq.clinic.domain.model.*
import io.mockk.MockKAnnotations
import io.mockk.every
import io.mockk.impl.annotations.RelaxedMockK
import io.mockk.junit5.MockKExtension
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.assertDoesNotThrow
import org.junit.jupiter.api.assertThrows
import org.junit.jupiter.api.extension.ExtendWith
import java.time.LocalDateTime

@ExtendWith(MockKExtension::class)
class AppointmentManagerTest {
    @RelaxedMockK
    lateinit var appointmentRequest: AppointmentRequest
    @RelaxedMockK
    lateinit var patientService: PatientService
    @RelaxedMockK
    lateinit var doctorService: DoctorService
    @RelaxedMockK
    lateinit var appointmentService: AppointmentService

    var appointmentManager: AppointmentManager

    private var doctor: Doctor
    private var patient: Patient

    init {
        MockKAnnotations.init(this, relaxUnitFun = true)

        doctor = Doctor(11, "Marek", "Zaradny", Specialty.DENTIST )
        val patientAddress = Address(1, "Krótka", "2", "Szczecin", "71-126")
        patient = Patient(22, "Ilona", "Mazurek", patientAddress)

        val appointment = Appointment(1, LocalDateTime.now(), 333, doctor, patient)
        every { appointmentService.createAppointment(appointment) } returns appointment

        appointmentManager = AppointmentManager(patientService, doctorService, appointmentService)
    }

    @Test
    fun shouldCreateAppointmentWithSuccess() {
        // given
        every { doctorService.isAvailable(any()) } returns true
        every { patientService.isAvailable(any()) } returns true
        // when
        assertDoesNotThrow { appointmentManager.makeAppointment(appointmentRequest) }
    }

    @Test
    fun shouldFailAppointmentCreationBecauseOfDoctorUnavailability() {
        // given
        every { doctorService.isAvailable(any()) } returns false
        every { patientService.isAvailable(any()) } returns true
        // when and then
        assertThrows<AppointmentCreationException> { appointmentManager.makeAppointment(appointmentRequest) }
    }

    @Test
    fun shouldFailAppointmentCreationBecauseOfPatientUnavailability() {
        // given
        every { doctorService.isAvailable(any()) } returns true
        every { patientService.isAvailable(any()) } returns false
        // when and then
        assertThrows<AppointmentCreationException> { appointmentManager.makeAppointment(appointmentRequest) }
    }
}
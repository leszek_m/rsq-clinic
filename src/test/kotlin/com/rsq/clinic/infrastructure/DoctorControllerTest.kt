package com.rsq.clinic.infrastructure

import com.rsq.clinic.domain.DoctorService
import com.rsq.clinic.domain.model.Doctor
import com.rsq.clinic.domain.model.Specialty
import io.mockk.Runs
import io.mockk.every
import io.mockk.junit5.MockKExtension
import io.mockk.just
import io.mockk.mockk
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.extension.ExtendWith
import org.skyscreamer.jsonassert.JSONAssert
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest
import org.springframework.boot.test.context.TestConfiguration
import org.springframework.context.annotation.Bean
import org.springframework.http.MediaType
import org.springframework.test.web.servlet.MockMvc
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*
import org.springframework.test.web.servlet.result.MockMvcResultMatchers.content
import org.springframework.test.web.servlet.result.MockMvcResultMatchers.status

@WebMvcTest(controllers = [DoctorController::class])
@ExtendWith(MockKExtension::class)
class DoctorControllerTest {

    // needed because Spring doesn't see MockK mocks (only Mockito @MockBean) when creating context for WebMvcTest
    @TestConfiguration
    class TestContextConfig {
        @Bean
        fun doctorService() = mockk<DoctorService>()
    }

    @Autowired
    lateinit var mockMvc: MockMvc
    @Autowired
    lateinit var doctorService: DoctorService

    @Test
    fun shouldCreateDoctor() {
        // given
        val doctor = Doctor(1, "Marek", "Zaradny", Specialty.DENTIST )
        val doctorAsJson = "{\"name\":\"Marek\",\"surname\":\"Zaradny\",\"specialty\":\"DENTIST\"}"
        every { doctorService.createDoctor(any()) } returns doctor
        // when
        val result = mockMvc.perform(post("/doctors").content(doctorAsJson).contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isCreated).andReturn()
        // then
        assertEquals(doctor.id.toString(), result.response.contentAsString)
    }

    @Test
    fun shouldGetDoctor() {
        // given
        val doctor = Doctor(1, "Michaela", "Quinn", Specialty.ORTHOPAEDIST )
        every { doctorService.getDoctor(doctor.id) } returns doctor
        // when
        val result = mockMvc.perform(get("/doctors/${doctor.id}").accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk)
                .andExpect(content().contentType(MediaType.APPLICATION_JSON)).andReturn()
        // then
        val expectedJson = "{\"id\": ${doctor.id},\"name\":\"Michaela\",\"surname\":\"Quinn\",\"specialty\":\"ORTHOPAEDIST\"}"
        JSONAssert.assertEquals(expectedJson, result.response.contentAsString, true)
    }

    @Test
    fun shouldUpdateDoctor() {
        // given
        val doctor = Doctor(1, "Gregory", "House", Specialty.PHYSIOTHERAPIST )
        val doctorAsJson = "{\"name\":\"Gregory\",\"surname\":\"House\",\"specialty\":\"PHYSIOTHERAPIST\"}"
        every { doctorService.updateDoctor(doctor) } returns doctor
        // when
        val result = mockMvc.perform(put("/doctors/${doctor.id}").content(doctorAsJson).contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk).andReturn()
        // then
        val expectedJson = "{\"id\": ${doctor.id},${doctorAsJson.drop(1)}"
        JSONAssert.assertEquals(expectedJson, result.response.contentAsString, true)
    }

    @Test
    fun shouldDeleteDoctor() {
        // given
        val doctor = Doctor(1, "John", "Dolittle", Specialty.PHYSIOTHERAPIST )
        every { doctorService.deleteDoctor(doctor.id) } just Runs
        // when and then
        mockMvc.perform(delete("/doctors/${doctor.id}").accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isNoContent)
    }
}
package com.rsq.clinic.domain

import com.rsq.clinic.domain.model.Appointment
import com.rsq.clinic.infrastructure.AppointmentRepository
import org.springframework.stereotype.Service

@Service
class AppointmentService(private val appointmentRepository: AppointmentRepository) {
    fun createAppointment(appointment: Appointment): Appointment {
        return appointmentRepository.save(appointment)
    }
}
package com.rsq.clinic.domain.model

import javax.persistence.*

@Entity
data class Address(
        @Id @GeneratedValue val id: Long = 0,
        val street: String,
        val homeNr: String,
        val city: String,
        val postCode: String) {
}
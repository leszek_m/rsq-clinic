package com.rsq.clinic.domain.model

import java.time.LocalDateTime
import javax.persistence.*

// TODO - maybe there should be fields like 'createdAt' and 'updatedAt'

@Entity
data class Appointment(
        @Id @GeneratedValue val id: Long = 0,
        var dateTime: LocalDateTime,
        var room: Int,
        @ManyToOne(fetch = FetchType.LAZY)
        @JoinColumn(name = "doctor_id")
        val doctor: Doctor,
        @ManyToOne(fetch = FetchType.LAZY)
        @JoinColumn(name = "patient_id")
        val patient: Patient) {
}
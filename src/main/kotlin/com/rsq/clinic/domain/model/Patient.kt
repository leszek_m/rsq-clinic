package com.rsq.clinic.domain.model

import com.fasterxml.jackson.annotation.JsonIgnore
import javax.persistence.*

// TODO - maybe Patient and Doctor should inherit from Person

@Entity
data class Patient(
        @Id @GeneratedValue
        val id: Long = 0,
        val name: String,
        val surname: String,
        @OneToOne(cascade = arrayOf(CascadeType.ALL))
        var address: Address,
        @JsonIgnore
        @OneToMany(mappedBy = "patient")
        val appointments: MutableCollection<Appointment> = mutableSetOf()){
}
package com.rsq.clinic.domain.model

import com.fasterxml.jackson.annotation.JsonIgnore
import javax.persistence.*

@Entity
data class Doctor(
        @Id @GeneratedValue val id: Long = 0,
        val name: String,
        val surname: String,
        @Enumerated(EnumType.STRING)
        val specialty: Specialty,
        @JsonIgnore
        @OneToMany(mappedBy = "doctor")
        val appointments: MutableCollection<Appointment> = mutableSetOf()) {
}
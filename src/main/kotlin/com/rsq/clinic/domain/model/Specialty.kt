package com.rsq.clinic.domain.model

enum class Specialty {
    DENTIST,
    PHYSIOTHERAPIST,
    ORTHOPAEDIST
}
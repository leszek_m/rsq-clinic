package com.rsq.clinic.domain

import com.rsq.clinic.domain.model.Patient
import com.rsq.clinic.infrastructure.PatientRepository
import org.springframework.stereotype.Service
import java.time.LocalDateTime
import java.time.temporal.ChronoUnit

@Service
class PatientService(private val patientRepository: PatientRepository) {
    fun createPatient(patient: Patient): Patient {
        return patientRepository.save(patient)
    }

    fun getPatient(id: Long): Patient {
        return patientRepository.getOne(id)
    }

    fun updatePatient(patient: Patient): Patient {
        val addressId = patientRepository.getOne(patient.id).address.id
        patient.address = patient.address.copy(id = addressId)
        return patientRepository.save(patient)
    }

    fun deletePatient(id: Long) {
        patientRepository.deleteById(id)
    }

    fun getAllPatients(): List<Patient> = patientRepository.findAll()

    fun isAvailable(time: LocalDateTime): Boolean {
        val roundedTime: LocalDateTime = time.truncatedTo(ChronoUnit.HOURS)
        val foundPatient: Patient? = patientRepository.findByIdAndAppointmentTime(roundedTime)
        if (foundPatient != null)
            return false
        return true
    }
}
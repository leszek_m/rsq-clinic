package com.rsq.clinic.domain.exceptions

class AppointmentCreationException(message: String?) : Exception(message) {
}
package com.rsq.clinic.domain

import com.rsq.clinic.domain.model.Doctor
import com.rsq.clinic.infrastructure.DoctorRepository
import org.springframework.stereotype.Service
import java.time.LocalDateTime
import java.time.temporal.ChronoUnit

@Service
class DoctorService(private val doctorRepository: DoctorRepository) {
    fun createDoctor(doctor: Doctor): Doctor {
        return doctorRepository.save(doctor)
    }

    fun getDoctor(id: Long): Doctor {
        return doctorRepository.getOne(id)
    }

    fun updateDoctor(doctor: Doctor): Doctor {
        return doctorRepository.save(doctor)
    }

    fun deleteDoctor(id: Long) {
        doctorRepository.deleteById(id)
    }

    fun isAvailable(time: LocalDateTime): Boolean {
        val roundedTime: LocalDateTime = time.truncatedTo(ChronoUnit.HOURS)
        val foundDoctor: Doctor? = doctorRepository.findByIdAndAppointmentTime(roundedTime)
        if (foundDoctor != null)
            return false
        return true
    }
}
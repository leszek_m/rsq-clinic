package com.rsq.clinic.application.dto

import java.time.LocalDateTime

// TODO - not sure how to avoid using DTO
data class AppointmentRequest(
        val dateTime: LocalDateTime,
        val room: Int,
        val patientId: Long,
        val doctorId: Long)
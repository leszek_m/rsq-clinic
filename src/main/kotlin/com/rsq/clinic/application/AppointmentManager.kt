package com.rsq.clinic.application

import com.rsq.clinic.domain.AppointmentService
import com.rsq.clinic.domain.DoctorService
import com.rsq.clinic.domain.PatientService
import com.rsq.clinic.domain.model.Appointment
import com.rsq.clinic.application.dto.AppointmentRequest
import com.rsq.clinic.domain.exceptions.AppointmentCreationException
import org.springframework.stereotype.Service
import java.time.LocalDateTime
import javax.transaction.Transactional

@Service
class AppointmentManager(
        private val patientService: PatientService,
        private val doctorService: DoctorService,
        private val appointmentService: AppointmentService) {

    @Transactional
    fun makeAppointment(appointmentRequest: AppointmentRequest): Appointment {
        checkForDoctorAndPatientAvailability(appointmentRequest.dateTime)
        val patient = patientService.getPatient(appointmentRequest.patientId)
        val doctor = doctorService.getDoctor(appointmentRequest.doctorId)
        val appointment = Appointment(dateTime = appointmentRequest.dateTime, room = appointmentRequest.room,
                patient = patient, doctor = doctor)
        return appointmentService.createAppointment(appointment)
    }

    private fun checkForDoctorAndPatientAvailability(time: LocalDateTime) {
        if (! doctorService.isAvailable(time))
            throw AppointmentCreationException("Doctor has already appointment at this time")
        if (! patientService.isAvailable(time))
            throw AppointmentCreationException("Patient has already appointment at this time")
    }
}
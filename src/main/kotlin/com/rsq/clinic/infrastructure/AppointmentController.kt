package com.rsq.clinic.infrastructure

import com.rsq.clinic.application.AppointmentManager
import com.rsq.clinic.application.dto.AppointmentRequest
import com.rsq.clinic.domain.model.Appointment
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RestController

@RestController
class AppointmentController(
        private val appointmentManager: AppointmentManager) {

    @PostMapping("/appointments")
    fun createAppointment(@RequestBody appointmentRequest: AppointmentRequest): ResponseEntity<Long> {
        var appointment: Appointment?
        try {
            appointment = appointmentManager.makeAppointment(appointmentRequest)
            return ResponseEntity<Long>(appointment.id, HttpStatus.CREATED)
        } catch (e: Exception) {
            // TODO - should be done with some general exception handler returning exception message with http status
            return ResponseEntity<Long>(-1, HttpStatus.CONFLICT)
        }
    }
}
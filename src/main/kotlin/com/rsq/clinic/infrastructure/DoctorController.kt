package com.rsq.clinic.infrastructure

import com.rsq.clinic.domain.DoctorService
import com.rsq.clinic.domain.model.Doctor
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.*

@RestController
@RequestMapping("/doctors")
class DoctorController(private val doctorService: DoctorService) {

    @PostMapping()
    fun createDoctor(@RequestBody doctor: Doctor): ResponseEntity<Long> {
        val createdDoctor = doctorService.createDoctor(doctor)
        return ResponseEntity<Long>(createdDoctor.id, HttpStatus.CREATED)
    }

    @GetMapping("/{id}")
    fun getDoctor(@PathVariable id: Long): ResponseEntity<Doctor> {
        return ResponseEntity<Doctor>(doctorService.getDoctor(id), HttpStatus.OK)
    }

    @PutMapping("/{id}")
    fun updateDoctor(@PathVariable id: Long, @RequestBody doctor: Doctor): ResponseEntity<Doctor> {
        val updatedDoctor = doctor.copy(id = id)
        return ResponseEntity<Doctor>(doctorService.updateDoctor(updatedDoctor), HttpStatus.OK)
    }

    @DeleteMapping("/{id}")
    fun deleteDoctor(@PathVariable id: Long): ResponseEntity<Doctor> {
        doctorService.deleteDoctor(id)
        return ResponseEntity<Doctor>(HttpStatus.NO_CONTENT)
    }
}
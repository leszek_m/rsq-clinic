package com.rsq.clinic.infrastructure

import com.rsq.clinic.domain.model.Appointment
import org.springframework.data.jpa.repository.JpaRepository

interface AppointmentRepository : JpaRepository<Appointment, Long> {
}
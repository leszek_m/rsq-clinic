package com.rsq.clinic.infrastructure

import com.rsq.clinic.domain.model.Patient
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.data.jpa.repository.Query
import org.springframework.data.repository.query.Param
import java.time.LocalDateTime

interface PatientRepository : JpaRepository<Patient, Long> {
    @Query("SELECT DISTINCT p FROM Patient p LEFT JOIN p.appointments app WHERE app.patient.id = p.id AND app.dateTime = :appointmentTime")
    fun findByIdAndAppointmentTime(@Param("appointmentTime") appointmentTime: LocalDateTime): Patient?
}
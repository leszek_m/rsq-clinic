package com.rsq.clinic.infrastructure

import com.rsq.clinic.domain.model.Doctor
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.data.jpa.repository.Query
import org.springframework.data.repository.query.Param
import java.time.LocalDateTime

interface DoctorRepository : JpaRepository<Doctor, Long> {
    @Query("SELECT DISTINCT doc FROM Doctor doc LEFT JOIN doc.appointments app WHERE app.doctor.id = doc.id AND app.dateTime = :appointmentTime")
    fun findByIdAndAppointmentTime(@Param("appointmentTime") appointmentTime: LocalDateTime): Doctor?
}
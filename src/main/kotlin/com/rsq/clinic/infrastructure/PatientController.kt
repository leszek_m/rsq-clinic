package com.rsq.clinic.infrastructure

import com.rsq.clinic.domain.PatientService
import com.rsq.clinic.domain.model.Patient
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.*

// TODO - refactor responses to HATEOAS, optionally add validation, error handling
@RestController
class PatientController(private val patientService: PatientService) {

    @PostMapping("/patients")
    fun createPatient(@RequestBody patient: Patient): ResponseEntity<Long> {
        val createdPatient = patientService.createPatient(patient)
        return ResponseEntity<Long>(createdPatient.id, HttpStatus.CREATED)
    }

    @GetMapping("/patients/{id}")
    fun getPatient(@PathVariable id: Long): ResponseEntity<Patient> {
        return ResponseEntity<Patient>(patientService.getPatient(id), HttpStatus.OK)
    }

    @PutMapping("/patients/{id}")
    fun updatePatient(@PathVariable id: Long, @RequestBody patient: Patient): ResponseEntity<Patient> {
        val updatedPatient = patient.copy(id = id)
        return ResponseEntity<Patient>(patientService.updatePatient(updatedPatient), HttpStatus.CREATED)
    }

    @DeleteMapping("/patients/{id}")
    fun deletePatient(@PathVariable id: Long): ResponseEntity<Patient> {
        patientService.deletePatient(id)
        return ResponseEntity<Patient>(HttpStatus.OK)
    }

    @GetMapping("/patients")
    fun getAllPatients(): ResponseEntity<Collection<Patient>> {
        return ResponseEntity<Collection<Patient>>(patientService.getAllPatients(), HttpStatus.OK)
    }
}